#!/bin/bash

# Generating a python3 venv

cd /home/ubuntu/security-orchestrator/SecurityEnablerProvider_RessourcePlanning
virtualenv  -p python2. venv_listenabler
#Activate the venv
source venv_listenabler/bin/activate
#Instasourll the pyxb in the venv
pip3 install PyXB==1.2.6

#install flacon
pip install falcon==1.3.0
#install gunicorn
pip install gunicorn==19.7.1

# install 
pip3 install requests

# after installing Cplex V12.5.1, install the python library
cd /opt/ibm/ILOG/CPLEX_Studio1251/cplex/python/x86-64_sles10_4.1/
sudo python setup.py install

#Add Cplex to PYTHONPATH
PYTHONPATH="${PYTHONPATH}:/opt/ibm/ILOG/CPLEX_Studio1251/cplex/python/x86-64_sles10_4.1"

export PYTHONPATH


cd /home/ubuntu/security-orchestrator/SecurityEnablerProvider_RessourcePlanning

gunicorn --reload SecurityEnablerProvider_RessourcePlanning.app
#run the main or curl
#curl -v localhost:8000/get_plugins?capabilities='Filtering_L3'
#curl -v localhost:8000/get_plugin?name='iptables'


