__author__ = "Dallal Belabed"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Dallal Belabed"]
__license__ = "GNU GPL"
__version__ = "0.0.1"
__maintainer__ = "Dallal Belabed"
__email__ = "dallal.Belabed@thales.com"
__status__ = "Development"

This python module implements a fast protopype of the security enabler provider and the Security Ressource Planning.
How to use:
	Run the server: gunicorn at localhost:8000
	Request plugin list by capabilities:  localhost:8000/get_plugins?capabilities='c1','c2'
	Request the Openstack flavors for each capabilites and fill the listenablers arrays AALTO SYSMODEL
	Request the Sysmodel to get the nodes available capacities and fill the nodes arrays
	Resquest the selectEnabler to get the best enabler from the listenablers
	Request plugin by name: curl localhost:8000/get_plugin?name=[PLUGIN_NAME]


#1. The get_plugins represents the API for getEnabler list. Please note that all the enabler have to be added to the repository plugin/ 

#2. The Security ressource planning module is reprensented by the "selectEnabler.py" class this module will be integrated in the Security Orchestrator

#3. The get_plugin represents the API for the get plugin, all the the plugins have to be added to the m2l_plugins folder

After following the next steps "you can also directly run the script environement.sh" Open a new terminal and run main.py.

# Generating a python3 venv
virtualenv  -p python3 venv_listenabler
#Activate the venv
source venv_listenabler/bin/activate

#Install Cplex V12.5.1 (change the CPLEX folder to conform with your distribution)
cd /opt/ibm/ILOG/CPLEX_Studio1251/cplex/python/x86-64_sles10_4.1/
sudo python setup.py install
#Add Cplex to PYTHONPATH
PYTHONPATH="${PYTHONPATH}:/opt/ibm/ILOG/CPLEX_Studio1251/cplex/python/x86-64_sles10_4.1"
export PYTHONPATH

YOU can run the script "environment.sh" to install the following modules
#Instasourll the pyxb in the venv
pip3 install PyXB==1.2.6

#install flacon
pip install falcon==1.3.0
#install gunicorn
pip install gunicorn==19.7.1

#instal request
sudo pip3 install request

