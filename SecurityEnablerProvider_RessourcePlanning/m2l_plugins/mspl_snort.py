# # -*- coding: utf-8 -*-
"""
This python module implements a MSPL->SNORT plugin inside the ANASTACIA European Project,
extending the MSPL language defined in secured project.
How to use:
	python3 mspl_snort.py [MSPL_FILE.xml]

"""
__author__ = "Dallal Belabed"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca", "Dallal Belabed"]
__license__ = "GPL"
__version__ = "0.0.2"
__maintainer__ = "Dallal Belabed"
__email__ = "dallal.belabed@thalesgroup.com"
__status__ = "Development"


import sys,os
import json
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
# Insert the parent path into the sys.path in order to import the mspl
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)
import mspl

"""
	Output example:
	{"output":{alert icmp any any -> any any (msg:"ICMP_DoS ATTACK 1"; detection_filter: track by_src, count 50, seconds 10; sid:100001; rev:001;) \n
alert icmp any any -> any any (msg:"ICMP_DoS ATTACK 2"; detection_filter: track by_src, count 100, seconds 10; sid:100002; rev:001;)"}}
"""

class ITResourceType(mspl.ITResourceType):

	def get_configuration(self):
		'Translate the ITResource element to SNORT configuration'
		capability_name = self.configuration.capability[0].Name
		logger.info("get_configuration capability_name...")
		if not capability_name in mspl.CapabilityType.itervalues():
			raise ValueError('The capability {} does not exist, please try with the current supported capabilities'.format(capability_name))
		# In filtering case, call translation for filtering Action and filtering conf condition
		try:
			capability_configuration_function = getattr(self, "get_{}_configuration".format(capability_name))
		except AttributeError as e:
			print("Currently, {} is not implemented.".format(capability_name))
			print(e)
			sys.exit(1)

		action, capability_enabler_conf = capability_configuration_function()
		# Build the enabler configuration, since the action is mandatory is not neccesary to check it
		enabler_conf = "{} {}".format(action, capability_enabler_conf if capability_enabler_conf else "")
		return enabler_conf

	def get_Filtering_L3_configuration(self):
		'Returns the configuration for Filtering_L4 capability'
		# For filtering it is only needed the first configuration rule
		try:
			configuration_rule = self.configuration.configurationRule[0]
		except:
			print("Filtering_L4 capability requires at least a configurationRule element")
			sys.exit(1)
		action = configuration_rule.configurationRuleAction.get_configuration()
		filtering_enabler_conf = configuration_rule.configurationCondition.get_configuration()
		
		return (action,filtering_enabler_conf)

	

class FilteringAction(mspl.FilteringAction):
	def get_configuration(self):
		'Returns the filtering action for SNORT'
		FILTERING_ACTION = {"Alert": "alert", "Log":"log", "Pass":"pass"}
		return FILTERING_ACTION.get(self.FilteringActionType,"DROP")

class FilteringConfigurationCondition(mspl.FilteringConfigurationCondition):
	def get_configuration(self):
		'Returns the traslated filtering SNORT parameters'
		filtering_enabler_conf = ""
		packet_filter_condition = self.packetFilterCondition
		State_ful_Condition = self.statefulCondition
		time_Condition = self.timeCondition
		if not packet_filter_condition:
			return None
		if not State_ful_Condition:
			return None
		if not time_Condition:
			return None

		filtering_enabler_conf+=" {}".format(packet_filter_condition.ProtocolType) if packet_filter_condition.ProtocolType else ""
		filtering_enabler_conf+=" {}".format(packet_filter_condition.SourceAddress) if packet_filter_condition.SourceAddress else ""
		filtering_enabler_conf+=" -> {} ".format(packet_filter_condition.SourcePort) if packet_filter_condition.SourcePort else ""
		filtering_enabler_conf+=" {} ".format(packet_filter_condition.DestinationAddress) if packet_filter_condition.DestinationAddress else ""
		filtering_enabler_conf+=" {}".format(packet_filter_condition.DestinationPort) if packet_filter_condition.DestinationPort else ""


		filtering_enabler_conf+=" (msg:\"{}\"".format(State_ful_Condition.Message) if State_ful_Condition.Message else ""
		filtering_enabler_conf+="; detection_filter:{}".format(State_ful_Condition.Detection_filter) if State_ful_Condition.Detection_filter else ""
		filtering_enabler_conf+=", count {}".format(State_ful_Condition.Count) if State_ful_Condition.Count else ""
		filtering_enabler_conf+=", seconds {}".format(time_Condition.Time) if time_Condition.Time else ""
		filtering_enabler_conf+="; sid:{}".format(State_ful_Condition.Sid) if State_ful_Condition.Sid else ""
		filtering_enabler_conf+="; rev:{}".format(State_ful_Condition.Rev) if State_ful_Condition.Rev else ""
		filtering_enabler_conf+=";)" 
		

		return filtering_enabler_conf if filtering_enabler_conf else None

class M2LPlugin:
	'SNORT Medium to low plugin implementation'

	def get_configuration(self,mspl_source):
		'Return the SNORT configuration from the mspl_source'
		# Customize the pyxb generated classes with our own behavior
		global mspl
		mspl.ITResourceType._SetSupersedingClass(ITResourceType)
		mspl.FilteringAction._SetSupersedingClass(FilteringAction)
		mspl.FilteringConfigurationCondition._SetSupersedingClass(FilteringConfigurationCondition)
		#Load the xml
		it_resource = mspl.CreateFromDocument(mspl_source)
		# Start the parser process using the xml root element
		return it_resource.get_configuration()

if __name__ == "__main__":

	#output file 
	outputfile = "local.rules";
	source = open(outputfile, "w")
	# Pretty print
	separator = "*"
	separator_group = separator*5
	title = "SNORT CONFIGURATION"
	print("\n{}{}{}".format(separator_group,title,separator_group))

	for i in range (1,len(sys.argv)):
		print (sys.argv[i])
		xml_file = sys.argv[i]
		# Instantiate the plugin
		m2lplugin = M2LPlugin()
		logger.info("Reading mspl file...")
		xml_source = open(xml_file).read()
		logger.info("Translating mspl to SNORT...")
		enabler_configuration = m2lplugin.get_configuration(xml_source)
		print(enabler_configuration)
		source.write(enabler_configuration+ "\n")
	print("{}{}{}\n".format(separator_group,separator*len(title),separator_group))	
	print ("Snort file path: " + os.path.abspath(outputfile))
	
	source.close()


