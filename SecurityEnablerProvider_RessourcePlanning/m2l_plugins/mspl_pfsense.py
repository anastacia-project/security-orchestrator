# # -*- coding: utf-8 -*-
"""
This python module implements a MSPL->PFSense plugin inside the ANASTACIA European Project,
extending the MSPL language defined in secured project.
How to use:
	python3 mspl_PFSense.py [MSPL_FILE.xml]

"""
__author__ = "Dallal Belabed"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca", "Dallal Belabed"]
__license__ = "GPL"
__version__ = "0.0.2"
__maintainer__ = "Dallal Belabed"
__email__ = "dallal.belabed@thalesgroup.com"
__status__ = "Development"


import sys,os
import json
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
# Insert the parent path into the sys.path in order to import the mspl
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)
import mspl

"""
	Output example:
	PFSense instalation instructions
"""

class ITResourceType(mspl.ITResourceType):

	def get_configuration(self):
		'Translate the ITResource element to PFSense configuration'
		capability_name = self.configuration.capability[0].Name
		logger.info("get_configuration capability_name...")
		if not capability_name in mspl.CapabilityType.itervalues():
			raise ValueError('The capability {} does not exist, please try with the current supported capabilities'.format(capability_name))
		# In filtering case, call translation for  filtering conf condition
		try:
			capability_configuration_function = getattr(self, "get_{}_configuration".format(capability_name))
		except AttributeError as e:
			print("Currently, {} is not implemented.".format(capability_name))
			print(e)
			sys.exit(1)

		capability_enabler_conf = capability_configuration_function()
		
		enabler_conf = "{}".format(capability_enabler_conf if capability_enabler_conf else "")
		return enabler_conf

	def get_Filtering_L3_configuration(self):
		'Returns the configuration for Filtering_L4 capability'
		# For filtering it is only needed the first configuration rule
		try:
			configuration_rule = self.configuration.configurationRule[0]
		except:
			print("Filtering_L4 capability requires at least a configurationRule element")
			sys.exit(1)
		filtering_enabler_conf = configuration_rule.configurationCondition.get_configuration()
		
		return (filtering_enabler_conf)

	



class FilteringConfigurationCondition(mspl.FilteringConfigurationCondition):
	def get_configuration(self):
		'Returns the traslated filtering PFSense parameters'
		filtering_enabler_conf = ""
		State_ful_Condition = self.statefulCondition
		if not State_ful_Condition:
			return None
	
		filtering_enabler_conf+="glance image-create --name \"{}\"".format(State_ful_Condition.Imgname) if State_ful_Condition.Imgname else ""
		filtering_enabler_conf+=" --file {}".format(State_ful_Condition.ImgFile) if State_ful_Condition.ImgFile else ""
		filtering_enabler_conf+=" --disk-format ova --container-format bare --progress \n"
		filtering_enabler_conf+="nova interface-attach --net-id {}".format(State_ful_Condition.PortId) if State_ful_Condition.PortId else ""
		filtering_enabler_conf+=" {} \n".format(State_ful_Condition.Imgname) if State_ful_Condition.Imgname else ""
		
		

		return filtering_enabler_conf if filtering_enabler_conf else None

class M2LPlugin:
	'PFSense Medium to low plugin implementation'

	def get_configuration(self,mspl_source):
		'Return the PFSense configuration from the mspl_source'
		# Customize the pyxb generated classes with our own behavior
		global mspl
		mspl.ITResourceType._SetSupersedingClass(ITResourceType)
		mspl.FilteringConfigurationCondition._SetSupersedingClass(FilteringConfigurationCondition)
		#Load the xml
		it_resource = mspl.CreateFromDocument(mspl_source)
		# Start the parser process using the xml root element
		return it_resource.get_configuration()

if __name__ == "__main__":

	#output file 
	outputfile = "install.sh";
	source = open(outputfile, "w")
	script_enabler_conf ="#!/bin/bash \n"
	
	# Pretty print
	separator = "*"
	separator_group = separator*5
	title = "PFSense CONFIGURATION"
	print("\n{}{}{}".format(separator_group,title,separator_group))

	xml_file = sys.argv[1]
	# Instantiate the plugin
	m2lplugin = M2LPlugin()
	logger.info("Reading mspl file...")
	xml_source = open(xml_file).read()
	logger.info("Translating mspl to PFSense...")
	script_enabler_conf+= m2lplugin.get_configuration(xml_source)
	print(script_enabler_conf)
	source.write(script_enabler_conf+ "\n")
	print("{}{}{}\n".format(separator_group,separator*len(title),separator_group))	
	print ("PFSense file path: " + os.path.abspath(outputfile))
	
	source.close()


