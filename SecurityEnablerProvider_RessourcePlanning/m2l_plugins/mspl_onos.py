# # -*- coding: utf-8 -*-
"""
This python module implements a MSPL->ONOS plugin inside the ANASTACIA European Project,
extending the MSPL language defined in secured project.
How to use:
	python3 mspl_ONOS.py [MSPL_FILE.xml]

"""
__author__ = "Dallal Belabed"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca", "Dallal Belabed"]
__license__ = "GPL"
__version__ = "0.0.2"
__maintainer__ = "Dallal Belabed"
__email__ = "dallal.belabed@thalesgroup.com"
__status__ = "Development"


import sys,os
import json
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
# Insert the parent path into the sys.path in order to import the mspl
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)
import mspl

"""
	Output example:
	{Source_Address Destination_Address  IntermediateAddress
"""

class ITResourceType(mspl.ITResourceType):

	def get_configuration(self):
		'Translate the ITResource element to ONOS configuration'
		capability_name = self.configuration.capability[0].Name
		logger.info("get_configuration capability_name...")
		print (capability_name)
		if not capability_name in mspl.CapabilityType.itervalues():
			raise ValueError('The capability {} does not exist, please try with the current supported capabilities'.format(capability_name))
		# In filtering case, call translation for filtering Action and filtering conf condition
		try:
			capability_configuration_function = getattr(self, "get_{}_configuration".format(capability_name))
		except AttributeError as e:
			print("Currently, {} is not implemented.".format(capability_name))
			print(e)
			sys.exit(1)

		action, capability_enabler_conf = capability_configuration_function()
		# Build the enabler configuration, since the action is mandatory is not neccesary to check it
		enabler_conf = "{}".format(capability_enabler_conf if capability_enabler_conf else "")
		return enabler_conf

	def get_Traffic_Divert_configuration(self):
		'Returns the configuration for Filtering_L4 capability'
		# For filtering it is only needed the first configuration rule
		try:
			configuration_rule = self.configuration.configurationRule[0]
			print (configuration_rule)
		except:
			print("ONOS configuration capability requires at least a configurationRule element")
			sys.exit(1)
		
		action = configuration_rule.configurationRuleAction.get_configuration()
		print (action)

		filtering_enabler_conf = configuration_rule.configurationCondition.get_configuration()
		
		return (action, filtering_enabler_conf)

class TrafficDivertAction(mspl.TrafficDivertAction):
	def get_configuration(self):
		'Returns the filtering action for Traffic_Divert'
		TrafficDivert_Action = {"FORWARD": "ONOS", "DynamicRouting":"Quagga", "Nat":"nat"}
		return TrafficDivert_Action.get(self.TrafficDivertActionType,"ONOS")

class TrafficDivertConfigurationCondition(mspl.TrafficDivertConfigurationCondition):
	def get_configuration(self):
		'Returns the traslated filtering ONOS parameters'
		filtering_enabler_conf = ""
		
		Src_Packet_DivertCondition = self.SrcPacketDivertCondition
		Srcpacket_Filter_Condition = Src_Packet_DivertCondition.packetFilterCondition
		Dst_Packet_DivertCondition = self.DstPacketDivertCondition
		Dstpacket_Filter_Condition = Dst_Packet_DivertCondition.packetFilterCondition
		
		
		if not Src_Packet_DivertCondition:
			return None
		if not Dst_Packet_DivertCondition:
			return None

		filtering_enabler_conf+=" {}".format(Srcpacket_Filter_Condition.SourceAddress) if Srcpacket_Filter_Condition.SourceAddress else ""
		filtering_enabler_conf+=" {} ".format(Srcpacket_Filter_Condition.DestinationAddress) if Srcpacket_Filter_Condition.DestinationAddress else ""
		filtering_enabler_conf+=" {}".format(Dstpacket_Filter_Condition.DestinationAddress) if Dstpacket_Filter_Condition.DestinationAddress else ""


		

		return filtering_enabler_conf if filtering_enabler_conf else None

class M2LPlugin:
	'ONOS Medium to low plugin implementation'

	def get_configuration(self,mspl_source):
		'Return the ONOS configuration from the mspl_source'
		# Customize the pyxb generated classes with our own behavior
		global mspl
		mspl.ITResourceType._SetSupersedingClass(ITResourceType)
		mspl.TrafficDivertAction._SetSupersedingClass(TrafficDivertAction)
		mspl.TrafficDivertConfigurationCondition._SetSupersedingClass(TrafficDivertConfigurationCondition)
		#Load the xml
		it_resource = mspl.CreateFromDocument(mspl_source)
		# Start the parser process using the xml root element
		return it_resource.get_configuration()

if __name__ == "__main__":

	#output file 
	outputfile = "onosConf.txt";
	source = open(outputfile, "w")
	# Pretty print
	separator = "*"
	separator_group = separator*5
	title = "ONOS CONFIGURATION"
	print("\n{}{}{}".format(separator_group,title,separator_group))

	xml_file = sys.argv[1]
	# Instantiate the plugin
	m2lplugin = M2LPlugin()
	logger.info("Reading mspl file...")
	xml_source = open(xml_file).read()
	logger.info("Translating mspl to ONOS...")
	enabler_configuration = m2lplugin.get_configuration(xml_source)
	print(enabler_configuration)
	source.write(enabler_configuration+ "\n")
	print("{}{}{}\n".format(separator_group,separator*len(title),separator_group))	
	print ("ONOS file path: " + os.path.abspath(outputfile))
	
	source.close()


