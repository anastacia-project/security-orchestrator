# # -*- coding: utf-8 -*-
"""
This python module implements the formatter for a enabler configuration translation output.
The format will follow the next sintax:
	{"output":{"enabler_conf":"[ENABLER_CONFIGURATION]"}}

NOTICE: NOT IN USE, CURRENTLY IS EMBEDED DIRECTLY ON THE PLUGIN CLASS AS FUNCTION.
"""
__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Development"

import json

"""
{"output":{"psa_config":"*filter\n:INPUT ACCEPT [0:0]\n:OUTPUT ACCEPT [0:0]\n:FORWARD ACCEPT [0:0]\n-A FORWARD -p TCP -m time --timestart 08:00 --timestop 19:00 -j DROP\n-A FORWARD -p UDP -m time --timestart 08:00 --timestop 19:00 -j DROP\nCOMMIT\n"}}
"""

class EnablerConfFormatter:
	def __init__(self,enabler_conf):
		self.enabler_conf = enabler_conf


class JsonEnablerConfFormatter(EnablerConfFormatter):

	def __str__(self):
		return json.dumps({"output":{"enabler_conf":self.enabler_conf}})

