# # -*- coding: utf-8 -*-
"""
This python module implements a MSPL->Quagga plugin inside the ANASTACIA European Project,
extending the MSPL language defined in secured project.
How to use:
	python3 mspl_Quagga.py [MSPL_FILE.xml]

"""
__author__ = "Dallal Belabed"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca", "Dallal Belabed"]
__license__ = "GPL"
__version__ = "0.0.2"
__maintainer__ = "Dallal Belabed"
__email__ = "dallal.belabed@thalesgroup.com"
__status__ = "Development"


import sys,os
import json
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
# Insert the parent path into the sys.path in order to import the mspl
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)
import mspl

"""
	Output example: Quagga configurations files plus a script to launch them using neutron
	venv_plugins/m2l_plugins/daemons
	venv_plugins/m2l_plugins/zebra.conf
	venv_plugins/m2l_plugins/bgpd.conf
	venv_plugins/m2l_plugins/scripQuagga.sh
	venv_plugins/m2l_plugins/scripInstall.sh
"}}
"""

class ITResourceType(mspl.ITResourceType):

	def get_configuration(self):
		'Translate the ITResource element to Quagga configuration'
		capability_name = self.configuration.capability[0].Name
		logger.info("get_configuration capability_name...")
		if not capability_name in mspl.CapabilityType.itervalues():
			raise ValueError('The capability {} does not exist, please try with the current supported capabilities'.format(capability_name))
		try:
			capability_configuration_function = getattr(self, "get_{}_configuration".format(capability_name))
		except AttributeError as e:
			print("Currently, {} is not implemented.".format(capability_name))
			print(e)
			sys.exit(1)

		action, deamons_enabler_conf, zebra_enabler_conf ,  bgpd_enabler_conf, script_enabler_conf2, server  = capability_configuration_function()
		# Build the enabler configuration, since the action is mandatory is not neccesary to check it
		#enabler_conf = "{} {}".format(action, capability_enabler_conf if capability_enabler_conf else "")
		return deamons_enabler_conf, zebra_enabler_conf ,  bgpd_enabler_conf, script_enabler_conf2, server

	def get_Traffic_Divert_configuration(self):
		'Returns the configuration for Forwarding capability'
		try:
			configuration_rule = self.configuration.configurationRule[0]
		except:
			print("Quagga capability requires at least a configurationRule element")
			sys.exit(1)
		action = configuration_rule.configurationRuleAction.get_configuration()
		deamons_enabler_conf, zebra_enabler_conf ,  bgpd_enabler_conf, script_enabler_conf2, server  = configuration_rule.configurationCondition.get_configuration()
		
		return (action,deamons_enabler_conf, zebra_enabler_conf ,  bgpd_enabler_conf, script_enabler_conf2, server )

	

class TrafficDivertAction(mspl.TrafficDivertAction):
	def get_configuration(self):
		'Returns the filtering action for Traffic_Divert'
		TrafficDivert_Action = {"FORWARD": "ONOS", "DynamicRouting":"Quagga", "Nat":"nat"}
		return TrafficDivert_Action.get(self.TrafficDivertActionType,"ONOS")

class TrafficDivertConfigurationCondition(mspl.TrafficDivertConfigurationCondition):
	def get_configuration(self):
		'Returns the traslated filtering Quagga parameters'
		deamons_enabler_conf = ""
		zebra_enabler_conf = ""
		bgpd_enabler_conf = ""
		script_enabler_conf2 = ""
		router_condition = self.NetworkConfigration.routerConfiguration
		#protocol_condition = router_condition = self.NetworkConfigration.ProtocolConfiguration
		
		
		if not router_condition:
			return None

		deamons_enabler_conf += "zebra={} \n".format(router_condition.Zebra) if router_condition.Zebra else ""
		deamons_enabler_conf += "bgpd={} \n".format(router_condition.Bgpd) if router_condition.Bgpd else ""
		deamons_enabler_conf += "ospfd={} \n".format(router_condition.Ospfd) if router_condition.Ospfd else ""
		deamons_enabler_conf += "ospf6d={} \n".format(router_condition.Ospf6d) if router_condition.Ospf6d else ""
		deamons_enabler_conf += "ripd={} \n".format(router_condition.Ripd) if router_condition.Ripd else ""
		deamons_enabler_conf += "ripngd={} \n".format(router_condition.Ripngd) if router_condition.Ripngd else ""
		deamons_enabler_conf += "isisd={} \n".format(router_condition.Isisd) if router_condition.Isisd else ""

		zebra_enabler_conf+="# Zebra configuration \n# name of the router \nhostname {}\n".format(router_condition.Hostname) if router_condition.Hostname else "" 
		zebra_enabler_conf+="password {} \n \n# log \nlog file /var/log/quagga/zebra.log\n".format(router_condition.Password) if router_condition.Password else ""
		
		bgpd_enabler_conf += "# declare a router with local-as 1000 \nrouter bgp {} \n".format(router_condition.Router) if router_condition.Router else ""
		bgpd_enabler_conf += "# expose networks \nneighbor {}".format(router_condition.Neighbor) if router_condition.Neighbor else ""
		bgpd_enabler_conf += " remote-as {}\n".format(router_condition.RemoteAS) if router_condition.RemoteAS else ""
		bgpd_enabler_conf += "network {}/".format(router_condition.LocalNetwork) if router_condition.LocalNetwork else ""
		bgpd_enabler_conf += "{}\n".format(router_condition.LocalNetworkNetmask) if router_condition.LocalNetworkNetmask else ""
		bgpd_enabler_conf += "network {}/".format(router_condition.RemoteNetwork) if router_condition.RemoteNetwork else ""
		
		bgpd_enabler_conf += "{}\n".format(router_condition.RemoteNetworkNetmask) if router_condition.RemoteNetworkNetmask else ""
		bgpd_enabler_conf +="log file /var/log/quagga/bgpd.log \ndebug bgp events \ndebug bgp filters \ndebug bgp fsm \ndebug bgp keepalives \ndebug bgp updates"

		server = "{}\n".format(router_condition.Server) if router_condition.Server else ""
		
		
		State_ful_Condition = self.SrcPacketDivertCondition.statefulCondition
		if not State_ful_Condition:
			return None
	
		script_enabler_conf2+="glance image-create --name \"{}\"".format(State_ful_Condition.Imgname) if State_ful_Condition.Imgname else ""
		script_enabler_conf2+=" --file {}".format(State_ful_Condition.ImgFile) if State_ful_Condition.ImgFile else ""
		script_enabler_conf2+=" --disk-format ova --container-format bare --progress \n"
		script_enabler_conf2+="nova interface-attach --net-id {}".format(State_ful_Condition.PortId) if State_ful_Condition.PortId else ""
		script_enabler_conf2+=" {}".format(State_ful_Condition.Imgname) if State_ful_Condition.Imgname else ""
		
		return (deamons_enabler_conf, zebra_enabler_conf ,  bgpd_enabler_conf, script_enabler_conf2, server )

		#return deamons_enabler_conf
#, zebra_enabler_conf ,  bgpd_enabler_conf , script_enabler_conf)

class M2LPlugin:
	'ONOS Medium to low plugin implementation'

	def get_configuration(self,mspl_source):
		'Return the ONOS configuration from the mspl_source'
		# Customize the pyxb generated classes with our own behavior
		global mspl
		mspl.ITResourceType._SetSupersedingClass(ITResourceType)
		mspl.TrafficDivertAction._SetSupersedingClass(TrafficDivertAction)
		mspl.TrafficDivertConfigurationCondition._SetSupersedingClass(TrafficDivertConfigurationCondition)
		#Load the xml
		
		it_resource = mspl.CreateFromDocument(mspl_source)
		
		# Start the parser process using the xml root element
		

		return it_resource.get_configuration()

if __name__ == "__main__":

	#output files 
	outputfile = "daemons";
	outputfile1 = "zebra.conf";
	outputfile2 = "bgpd.conf";
	outputfile3 =	"scripQuagga.sh"
	outputfile4 =	"scripInstall.sh"
	
	source = open(outputfile, "w")
	source1 = open(outputfile1, "w")
	source2 = open(outputfile2, "w")
	source3 = open(outputfile3, "w")
	source4 = open(outputfile4, "w")

	

	# Pretty print
	separator = "*"
	separator_group = separator*5
	title = "Quagga CONFIGURATION"
	print("\n{}{}{}".format(separator_group,title,separator_group))

	for i in range (1,len(sys.argv)):
		
		xml_file = sys.argv[i]
		
		# Instantiate the plugin
		m2lplugin = M2LPlugin()
		logger.info("Reading mspl file...")
		xml_source = open(xml_file).read()
		logger.info("Translating mspl to Quagga...")
		enabler_configurationDemons, enabler_configurationZebra, enabler_configurationBgpd, script_enabler_conf2, server = m2lplugin.get_configuration(xml_source)
		#enabler_configurationDemons = m2lplugin.get_configuration(xml_source)
		source.write(enabler_configurationDemons+ "\n")
		source1.write(enabler_configurationZebra+ "\n")
		source2.write(enabler_configurationBgpd+ "\n")

		script_enabler_conf ="#!/bin/bash \n"
		script_enabler_conf += "\n wget {}/{} ".format(server,outputfile)
		script_enabler_conf += "\n cp {} /etc/quagga/ \n".format(outputfile)

		script_enabler_conf += "wget {}/{} \n".format(server,outputfile1)
		script_enabler_conf += "\n cp {} /etc/quagga/ \n".format(outputfile1)		
		
		script_enabler_conf += "wget {}/{} ".format(server,outputfile2)
		script_enabler_conf += "\n cp {} /etc/quagga/ \n".format(outputfile2)
	
		source3.write(script_enabler_conf)
		script_enabler_conf2+=" "+ os.path.abspath(outputfile3) 
		source4.write(script_enabler_conf2)
		
		print (script_enabler_conf)
		print (script_enabler_conf2)
	print("{}{}{}\n".format(separator_group,separator*len(title),separator_group))	
	print ("Quagga path files: \n" + os.path.abspath(outputfile) + "\n"+os.path.abspath(outputfile1) + "\n" +os.path.abspath(outputfile2) + "\n"+ os.path.abspath(outputfile3) + "\n"+ os.path.abspath(outputfile4) + "\n" )
	
	source.close()


