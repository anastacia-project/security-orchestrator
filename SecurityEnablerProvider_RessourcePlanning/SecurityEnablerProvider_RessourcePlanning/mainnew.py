#/usr/bin/env python3
__author__ = "Dallal Belabed"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Dallal Belabed"]
__license__ = "GNU GPL"
__version__ = "0.0.1"
__maintainer__ = "Dallal Belabed"
__email__ = "dallal.Belabed@thalesgroup.com"
__status__ = "Development"
"""
This python module implements a fast protopype of the security enabler provider.
How to use:
	Run the server: gunicorn at localhost:8000
	Request plugin list by capabilities:  localhost:8000/get_plugins?capabilities='c1','c2'
	Request the Openstack flavors for each capabilites and fill the listenablers arrays AALTO SYSMODEL
	Request the Sysmodel to get the nodes available capacities and fill the nodes arrays
	Resquest the selectEnabler to get the best enabler from the listenablers
	Request plugin by name: curl localhost:8000/get_plugin?name=[PLUGIN_NAME]

"""
import os
import sys
import json
import falcon
import cplex
import requests
#import app 
from enablers import Enablers
from nodes import Nodes
from selectEnabler import SelectPlugin


def main(argv):

	#******************************* GET LIST ENABLERS *****************************************************#
   	#Run guicorn os.system("gunicorn -b localhost:8000 folder:app --reload" or "gunicorn --reload folder.app")
	#Request plugin list by capabilities
	r= requests.get("http://127.0.0.1:8000/get_plugins", params = {'capabilities':'Filtering_L3'})
	#print (r)
	print(json.loads(r.text))
	#print (json.dumps(candidates))
	listEnblerCap = json.loads(r.text)
	
	for enb in listEnblerCap:
		print ("enabler")
		print (enb)


	#*******************************add capability
	payload = (('key1', 'value1'), ('key1', 'value2'))
	r = requests.post('http://127.0.0.1:8000/post', data = payload)
	print (r.text)

	#*******************************delete capability to be developed
	# r = requests.post('http://127.0.0.1:8000/delete_plugin', data = {'key':'value'})
	
		
	#******************************* RESSSOURCES PLUNNING *****************************************************#
   	
	# yacine has to replace the following tables by getflavor for list enablers
	# i.e listenablers = getflavor(listEnblerCap)
	# yacine replace the listnodes by the nodes from sysmodel

	#nbEL = sys.argv[1:]
	#nbE =
	#print(nbE)
	#json_input = '{"persons": [{"name": "Brian", "city": "Seattle"}, {"name": "David", "city": "Amsterdam"} ] }'
	nodejson = '{"nodes": [{"name": "node1", "cpu": 20,  "ram": 20, "hdd": 100, "nic": 10}, {"name": "node2", "cpu": 20, "ram": 20, "hdd": 100, "nic": 10} ]}'

	nodedic = json.loads(nodejson)
 
	vnfJson = '{"vnfs": [{"name": "onos", "cpu": 2,  "ram": 2, "hdd": 10, "nic": 1}, {"name": "snort", "cpu": 2,  "ram": 2, "hdd": 10, "nic": 1} , {"name": "ovs-fw", "cpu": 2,  "ram": 2, "hdd": 10, "nic": 1}, {"name": "iptable", "cpu": 2,  "ram": 2, "hdd": 10, "nic": 1}] }'

	vnfdic  = json.loads(vnfJson)
	listenablers=[]
	nbE= len(listEnblerCap) #nubmer of enabler and nodes just to test
	

	for i in range(0, nbE):
		for x in vnfdic['vnfs']:
			if (x['name']==listEnblerCap[i]):
				enab = Enablers(listEnblerCap[i], x['cpu'], x['ram'], x['hdd'], x['nic'])
				listenablers.append(enab)

	listenodes=[]
	for x in nodedic['nodes']:
		#enab = Nodes("Node{}".format(i), (i+1)*4, (i+1)*5, (i+1)*20, (i+1)*2)
		enab = Nodes(x['name'], x['cpu'], x['ram'], x['hdd'], x['nic'])
		listenodes.append(enab)

	print ("listenablers")
	print (listenablers)
	print ("listenodes")
	print (listenodes)

	#select the enabler
	
	SP =SelectPlugin()
	results = SP.SelectPlugin(listenablers, listenodes)
	r_enabler= results[0]
	r_node = results[1]
	print ("the enabler selected is: "+r_enabler.pName + " the node is "+ r_node.pName)
	
	#******************************* GET ENABLER PLUGIN ***********************************************#
   	
	r2= requests.get("http://127.0.0.1:8000/get_plugin", params = {'name':r_enabler.pName})
	print(json.loads(r2.text))

if __name__ == "__main__":
	
	main(sys.argv)
