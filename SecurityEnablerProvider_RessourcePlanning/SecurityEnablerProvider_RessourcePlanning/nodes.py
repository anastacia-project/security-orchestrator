#/usr/bin/env python3
__author__ = "Dallal Belabed"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Dallal Belabed"]
__license__ = "GNU GPL"
__version__ = "0.0.1"
__maintainer__ = "Dallal Belabed"
__email__ = "dallal.Belabed@thalesgroup.com"
__status__ = "Development"

class Nodes ():
#Node capacity requerements 4 core CPU, 2 GB RAM, 10 GB hdd, 1 NIC (any speed)
    def __init__(self, idd,  name, cpu, ram, hdd, nic):
        self.idd = idd
	self.pName = name #id
        self.pCPU = cpu
        self.pRAM = ram
        self.pHdd = hdd
        self.pNic = nic
