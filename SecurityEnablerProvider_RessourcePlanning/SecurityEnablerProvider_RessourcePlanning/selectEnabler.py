#/usr/bin/env python

__author__ = "Dallal Belabed"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Dallal Belabed"]
__license__ = "GNU GPL"
__version__ = "0.0.1"
__maintainer__ = "Dallal Belabed"
__email__ = "dallal.Belabed@thalesgroup.com"
__status__ = "Development"

import os
import sys
import json
import falcon

import logging

import cplex
import requests
from enablers import Enablers
from nodes import Nodes
from collections import defaultdict


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def json_dict_list(variables_file):
	d = defaultdict(list)
	with open(variables_file) as f:
		d = json.load(f)
	return d

class SelectPlugin:
	def on_get(self, req, resp):

		#******************************* GET LIST ENABLERS *****************************************************#
		#Run guicorn os.system("gunicorn -b localhost:8000 folder:app --reload" or "gunicorn --reload folder.app")
		#Request plugin list by capabilities
		#r= requests.get("http://127.0.0.1:8000/get_plugins", params = {'capabilities':'Filtering_L3'})
		#print (r)
		#print(json.loads(r.text))
		#print (json.dumps(candidates))
		#try:
		listEnablerCap = req.get_param_as_list("candidate",required=True)

		print ("listEnablerCap")
		print (listEnablerCap)

		nodejson = req.get_param("nodes",required=True)


		print ("select\n")
		print(nodejson)
		


		#******************************* GET LIST from system model *****************************************************#

	#	nodejson = '{"nodes": [{"name": "node1", "cpu": 20,  "ram": 20, "hdd": 100, "nic": 10}, {"name": "node2", "cpu": 20, "ram": 20, "hdd": 100, "nic": 10} ]}'
		#to be replace by request from AALTO
		#nodejson ='{"nodes": [{"id": "58d4d612-9cb1-492a-ae94-27813204d03a", "name": "node1", "cpu": 20,  "ram": 20, "hdd": 100}, {"id": "58d4d612-9cb1-492a-ae94-27813204d03a", "name": "node2", "cpu": 20, "ram": 20, "hdd": 100} ]}'
		nodedic = json.loads(nodejson)


		plugins_path = "plugins/securityEnablerRepoCapacity.json"

		vnfdic = json_dict_list(plugins_path)


		listenablers=[]
		nbE= len(listEnablerCap) #nubmer of enabler and nodes just to test


		for i in range(0, nbE):
			for x in vnfdic['vnfs']:
				if (x['name']==listEnablerCap[i]):
					enab = Enablers(i, listEnablerCap[i], x['cpu'], x['ram'], x['hdd'], x['nic'])
					listenablers.append(enab)

		i=0
		listnodes=[]
		for x in nodedic['nodes']:

			#enab = Nodes("Node{}".format(i), (i+1)*4, (i+1)*5, (i+1)*20, (i+1)*2)
			enab = Nodes(x['id'], x['name'], x['cpu'], x['ram'], x['hdd'], 1)
			listnodes.append(enab)
			i=i+1

		# Create an instance of a linear problem to solve
		problem = cplex.Cplex()

		# We want to find a minimize  our objective function
		problem.objective.set_sense(problem.objective.sense.minimize)

		#Create cplex arrays
		nbCs= 0
		nbVr=0
		constraints = []
		constraint_names =[]
		constraint_senses =[]
		rhs = []
		# The names of our variables
		E=[]
		N=[]
		i=0
		nbNodes= len(listnodes)
		for el in listenablers:
			E.append("e"+ str(i))
			i+=1

		i=0
		for el in listenablers:
			j=0
			for nd in listnodes:
				N.append("e_"+ str(i) + "n_"+ str(j))
				j+=1
			i+=1

		names = ["U"]+ E + N

		# The obective function. More precisely, the coefficients of the objective
		# function. Note that we are casting to floats.

		objective = [ 0 for el in names]

		objective[0]=1.0

		# Lower bounds. Since these are all zero, we could simply not pass them in as
		# all zeroes is the default.
		lower_bounds = [ 0 for el in names]

		# Upper bounds. The default here would be cplex.infinity, or 1e+20.
		upper_bounds = [1 for el in names]

		problem.variables.add(obj = objective,
				      lb = lower_bounds,
				      ub = upper_bounds,
				      names = names)
		#set type variables
		problem.variables.set_types(nbVr, problem.variables.type.continuous)
		nbVr+=1
		for i in range (1, len(names)) :
			problem.variables.set_types(i, problem.variables.type.binary)
			nbVr+=1

		# Constraints



		#first constraint Sum_e E =1
		constraint = [names, [0]+[1for el in E]+[0for n in N] ]
		constraints.append(constraint)
		constraint_names.append("C_SumE1"+str(nbCs))
		nbCs+=1
		constraint_senses.append("E")
		rhs.append(1)
		# In this second constraint, we can refer to the variables by their indices. Since
		# "x" was the first variable we added, "y" the second and "z" the third, this
		# then represents 3x + 4y + 4z
		#second constraint integrity  Ve sum_n N = 1
		i=0
		for el in listenablers:
			listenablerstmp = [0 for el in E]
			listenablerstmp[i]=-1
			j=0
			tmp =[0 for n in N]
			for nd in listnodes:
				tmp[(nbNodes*i)+j]= 1
				j+=1
			i+=1
			constraint = [names, [0]+listenablerstmp+ tmp ]
			constraints.append(constraint)
			constraint_names.append("C_sum_n"+str(nbCs))
			nbCs+=1
			constraint_senses.append("E")
			rhs.append(0)

		#Third constraint capacity  Ve Vn e cpu < n cpu
		i=0
		for enable in listenablers:
			j=0
			AlphaEnPCPU =[0for e in E]


			for node in listnodes:
				NodeCPU =[0for n in N]
				NodeCPU[i*nbNodes+j] = enable.pCPU

				j+=1
				constraint = [names, [-1*node.pCPU]+AlphaEnPCPU + NodeCPU]
				constraints.append(constraint)
				constraint_names.append("CPU_"+ str(nbCs))
				nbCs+=1
				constraint_senses.append("L")
				rhs.append(0)
			i+=1


		# In this thier constraint, RAM
		i=0
		for enable in listenablers:
			j=0
			AlphaEnPRAM =[0for e in E]


			for node in listnodes:
				NodeRAM =[0for n in N]
				NodeRAM[i*nbNodes+j] = enable.pRAM

				j+=1
				constraint = [names, [-1*node.pRAM]+AlphaEnPRAM + NodeRAM]
				constraints.append(constraint)
				constraint_names.append("RAM_"+str(nbCs))
				nbCs+=1
				constraint_senses.append("L")
				rhs.append(0)
			i+=1


		# In this thier constraint, Hdd
		i=0
		for enable in listenablers:
			j=0
			AlphaEnPHdd =[0for e in E]


			for node in listnodes:
				NodeHdd =[0for n in N]
				NodeHdd[i*nbNodes+j] = enable.pHdd

				j+=1
				constraint = [names, [-1*node.pHdd]+AlphaEnPHdd + NodeHdd]
				constraints.append(constraint)
				constraint_names.append("Hdd_"+str (nbCs))
				nbCs+=1
				constraint_senses.append("L")
				rhs.append(0)
			i+=1

		# In this thier constraint, Nic
		i=0
		for enable in listenablers:
			j=0
			AlphaEnPNic =[0for e in E]


			for node in listnodes:
				NodeNic =[0for n in N]
				NodeNic[i*nbNodes+j] = enable.pNic

				j+=1
				constraint = [names, [-1*node.pNic]+AlphaEnPNic + NodeNic]
				constraints.append(constraint)
				constraint_names.append("Nic_"+str(nbCs))
				nbCs+=1
				constraint_senses.append("L")
				rhs.append(0)
			i+=1








		problem.linear_constraints.add(lin_expr = constraints,
					       senses = constraint_senses,
					       rhs = rhs,
					       names = constraint_names)


		# Solve the problem
		problem.solve()

		# And print the solutions
		print (names)
		print(problem.solution.get_values())

		sol = problem.solution.get_values()
		deb_e =1
		enablerSelected = listenablers[0]
		for i in range (deb_e,len(E) ):
			if sol[i] ==1:
				enablerSelected= listenablers[i-1]
				print(enablerSelected.pName)

		deb_n = 1+len(E)
		selectedNode = listnodes[0]
		for el in listenablers:
			j=0
			for nd in listnodes:
				N.append("e_"+ str(i) + "n_"+ str(j))
				if sol[deb_n]==1:
					selectedNode = nd
					print(nd.pName)
				deb_n+=1
				j+=1
			i+=1
			print("objective")
			print (enablerSelected.pName, selectedNode.pName)
			print (problem.solution.get_objective_value())
			print ("end cplex")
		resp.status = falcon.HTTP_200  # This is the default status
		resp.body = (json.dumps([enablerSelected.pName, selectedNode.pName]))
		#except (FileNotFoundError,IOError) as e:
       		#resp.status = falcon.HTTP_500
       		#resp.body = str(e)

		return listenablers


#************************************
#***********************************
#************************************
#***********************************
