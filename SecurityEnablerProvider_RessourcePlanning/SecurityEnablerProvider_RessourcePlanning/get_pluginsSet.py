#/usr/bin/env python3

__author__ = "Dallal Belabed"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Dallal Belabed"]
__license__ = "GNU GPL"
__version__ = "0.0.1"
__maintainer__ = "Dallal Belabed"
__email__ = "dallal.Belabed@thalesgroup.com"

# Let's get this party started!
import csv
import sys
import pprint
import falcon
import json
import logging
from collections import defaultdict

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# Function to convert a csv file to a list of dictionaries.  Takes in one variable called "variables_file"
def csv_dict_list(variables_file):


    reader = csv.reader(open(variables_file, 'r'))
    d = defaultdict(list)

    for line in reader:
        k=line[0]
        tmpList=[]
        for l in line[1:]:
                if l !='':
                        tmpList.append(l)
        d[k]=tmpList
    return d




# Falcon follows the REST architectural style, meaning (among
# other things) that you think in terms of resources and state
# transitions, which map to HTTP verbs.
def PluginCatalog():

	        """Handles GET requests"""
	        # TODO: short the code
	        candidates = []
	        requested_capabilities = ['Filtering_L3','Traffic_Divert']
            #req.get_param_as_list("capabilities",required=True)

            #logger.info("Searching Security Enablers for {} capabilities".format(requested_capabilities))

	        logger.info("Searching Security Enablers for {} capabilities".format(requested_capabilities))
	        #try:
        	plugins_path = "plugins/securityEnablerRepo.csv"
        	PLUGIN_CAPABILITIES = csv_dict_list(plugins_path)
        	listoflist = []

        	for el in requested_capabilities:
                #print("capability {}".format(el))

        		listoflist.append(PLUGIN_CAPABILITIES[el])
        		logger.info ("PLUGIN_CAPABILITIES[el]")
                logger.info (PLUGIN_CAPABILITIES[el])
                logger.info  ("capability {}".format(el))

                    #if pl in setcap
        			#candidates.append(pl)
        	tcandidates = [set(a) for a in listoflist]
        	candidates = set.intersection(*tcandidates)


        	print("Selected Security Enablers: {}".format(candidates))
	       # except (FileNotFoundError,IOError) as e:
	        #	resp.status = falcon.HTTP_500
	        	#resp.body = str(e)
	        #return candidates
if __name__ == "__main__":
    PluginCatalog()
