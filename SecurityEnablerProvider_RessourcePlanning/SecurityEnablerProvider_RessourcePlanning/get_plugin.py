__author__ = "Alejandro Molina Zarca", "Dallal Belabed"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Dallal Belabed", "Alejandro Molina Zarca"]
__license__ = "GNU GPL"
__version__ = "0.0.1"
__maintainer__ = "Dallal Belabed"
__email__ = "dallal.Belabed@thalesgroup.com"
__status__ = "Development"

import logging
import json
import falcon

class Plugin(object):
    def on_get(self, req, resp):
        """Handles GET requests"""
        security_enabler_name = req.get_param("name",required=True)
        logging.basicConfig(level=logging.INFO)
        logger = logging.getLogger(__name__)
        logger.info("Searching a plugin for '{}' security enabler{}".format(security_enabler_name,"."*3))
        plugin_file_name = "mspl_{}.py".format(security_enabler_name)
        plugin_path = "m2l_plugins/{}".format(plugin_file_name)

        try:
            plugin = open(plugin_path,"r")
            resp.body = (json.dumps({"plugin_file_name":plugin_file_name,"plugin":plugin.read()}))
            logger.info("'{}' Security Enabler founded!. '{}' file was sent".format(security_enabler_name,plugin_file_name))
        except (FileNotFoundError,IOError) as e:
            resp.status = falcon.HTTP_500
            resp.body = str(e)
            return
