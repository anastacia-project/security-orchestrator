#/usr/bin/env python
__author__ = "Dallal Belabed"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Dallal Belabed"]
__license__ = "GNU GPL"
__version__ = "0.0.1"
__maintainer__ = "Dallal Belabed"
__email__ = "dallal.Belabed@thalesgroup.com"
__status__ = "Development"
"""
This python module implements a fast protopype of the security enabler provider.
How to use:
	Run the server: gunicorn at localhost:8000
	Request plugin list by capabilities:  localhost:8000/get_plugins?capabilities='c1','c2'
	Request the Openstack flavors for each capabilites and fill the listenablers arrays AALTO SYSMODEL
	Request the Sysmodel to get the nodes available capacities and fill the nodes arrays
	Resquest the selectEnabler to get the best enabler from the listenablers
	Request plugin by name: curl localhost:8000/get_plugin?name=[PLUGIN_NAME]

"""
import os
import sys
import json
import falcon
import cplex
import requests
#import app
from enablers import Enablers
from nodes import Nodes
from selectEnabler import SelectPlugin
#from request import get_resources
#from request import get_flavors


def main(argv):

	#******************************* GET LIST ENABLERS *****************************************************#
   	#Run guicorn os.system("gunicorn -b localhost:8000 folder:app --reload" or "gunicorn --reload folder.app")
	#Request plugin list by capabilities
	r= requests.get("http://127.0.0.1:8000/get_plugins", params = {'capabilities':'Filtering_L3'})
	print (r)
	print(json.loads(r.text))

	listEnblerCap = json.loads(r.text)
	listcandidate = listEnblerCap['candidate_security_enablers']


	#*******************************add capability todo and integrated with the policy interpreter
	#payload = (('key1', 'value1'), ('key1', 'value2'))
	#r = requests.post('http://127.0.0.1:8000/post', data = payload)

	#*******************************delete capability to be developed with the policy interpreter
	# r = requests.post('http://127.0.0.1:8000/delete_plugin', data = {'key':'value'})


	#******************************* RESSSOURCES PLUNNING *****************************************************#

	# AALTO has to replace the following tables by the VNF API for list enablers
	#AALTO has to replace the listnodes by the nodes from sysmodel

	nodejson ='{"nodes": [{"id": "58d4d612-9cb1-492a-ae94-27813204d03a", "name": "node1", "cpu": 20,  "ram": 20, "hdd": 100}, {"id": "58d4d612-9cb1-492a-ae94-27813204d03a", "name": "node2", "cpu": 20, "ram": 20, "hdd": 100} ]}'

	print ("nodejson")
	r2= requests.get("http://127.0.0.1:8000/selectEnabler", params = {'candidate':listcandidate, 'nodes': nodejson})


	print(json.loads(r2.text))

	results = json.loads(r2.text)
	r_enabler= results[0]
	r_node = results[1]
	print ("the enabler selected  are: "+r_enabler + " the node is "+ r_node)

	#******************************* GET ENABLER PLUGIN ***********************************************#



	r3= requests.get("http://127.0.0.1:8000/get_plugin", params = {'name':r_enabler})
	#print(json.loads(r2.text))

if __name__ == "__main__":

	main(sys.argv)
