__author__ = "Dallal Belabed"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Dallal Belabed"]
__license__ = "GNU GPL"
__version__ = "0.0.1"
__maintainer__ = "Dallal Belabed"
__email__ = "dallal.Belabed@thalesgroup.com"
__status__ = "Development"

#/usr/bin/env python3
import falcon
from .get_plugins import PluginCatalog
from .get_plugin import Plugin
from .selectEnabler import SelectPlugin
#import get_plugins 
#import get_plugin 

api = application = falcon.API()

get_plugins = PluginCatalog()
api.add_route('/get_plugins', get_plugins)

plugin = Plugin()
api.add_route('/get_plugin', plugin)

select_plugin=SelectPlugin()
api.add_route('/selectEnabler', select_plugin)
